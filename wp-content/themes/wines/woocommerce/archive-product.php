<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>

        <div class="main-slider">
            <div>
                <img class="img-responsive" src="<?php echo get_template_directory_uri()?>/assets/content/slide-1.jpg" alt="Slide 1">
            </div>
        </div>
        </div>
        <div class="main-content" id="shop-content">
            <div class="main-content-wrapper">
                <div class="row">
                    <div class="col-sm-6 visible-xs">
                        <img class="img-responsive center-block logo-vinum" src="<?php echo get_template_directory_uri()?>/assets/img/winum-logo.png" alt="Logo Vinum">
                    </div>
                    <div class="col-sm-3 col-xs-6">
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <img class="img-responsive center-block logo-vinum" src="<?php echo get_template_directory_uri()?>/assets/img/winum-logo.png" alt="Logo Vinum">
                    </div>
                    <div class="col-sm-3 col-xs-6">
                       
                    </div>
                </div>
                
                <?php  if ( $categories =  get_theme_categories()) : ?>
		            <ul class="filter-grid list-inline">
		                <?php 
		                	foreach ( $categories as $category ) : 
		                	$thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true ); 
		                	$image = wp_get_attachment_url( $thumbnail_id ); 
		                ?>
		                <li>
		                	<a href="#<?php echo $category->slug; ?>" class="product-image" data-group="<?php echo $category->slug; ?>" href="<?php echo get_term_link($category); ?>"><img class="img-responsive" src="<?php echo $image; ?>" alt="<?php echo $category->name; ?>" /></a>
		                	<a data-group="<?php echo $category->slug; ?>" href="#<?php echo $category->slug; ?>"><?php echo $category->name; ?></a>
		                </li>
		                <?php endforeach; ?>
		            </ul>
		        <?php endif; ?>

				<h3 class="main-title"><span>
					In wine <br />
					there's truth
				</span></h3>

				<?php  if ( $categories = get_theme_categories() ): ?>
					<?php foreach ( $categories as $category ) : ?>
						<div class="category-wrapper" id="<?php echo $category->slug; ?>">
							<div class="row">
								<?php $current_product = get_theme_products($category->term_id)?>

								<?php $index = 1; foreach ($current_product as $product) : 
									  $prod = wc_get_product( $product->ID );  
									  $price_wrapper = get_field('price_background', $product->ID); 
								?>
									<div class="col-sm-4">
										<div class="product-box">
											<div class="row">
												<div class="col-xs-4 product-img">
													<a class="open-big-image" href="<?php echo wp_get_attachment_url( get_post_thumbnail_id($prod->id)); ?>">
														<?php echo get_the_post_thumbnail($prod->id, 'product-loop');?>
														<span class="zoom-area"><i class="fa fa-search"></i></span>
													</a>
												</div>
												<div class="col-xs-8">
													<div class="product-meta">
														<h3>
															<a class="open-popup-link" href="#product-popup" data-id="<?php echo $prod->id; ?>"><?php echo $prod->get_title(); ?></a>
															<?php if ( ! $prod->is_in_stock() ) : ?>
																<span> sold out </span>
															<?php endif; ?>
														</h3>
														<?php echo theme_get_excerpt_by_id($prod->id); ?>
													</div>
													<ul>
														<li class="price" style="background-image: url('<?php echo $price_wrapper; ?> ')">
															<div class="price-wrapper">
																<?php echo $prod->get_price_html() ?>
															</div>
														</li>
														<li class="cart">
															<?php if ( $prod->is_in_stock() ) : 
																echo do_shortcode ('[add_to_cart id="' . $prod->id . '" style="border:none;" sku="' . $prod ->get_sku() . '"]');
															 endif; ?>
														</li>
													</ul>
												</div>
											</div>

										</div>
									</div>
									<?php if($index % 3 == 0) : ?>
										<div class="clearfix"></div>
									<?php endif; ?>
								<?php $index++; endforeach; wp_reset_postdata();?>
							</div>
						</div>
					<?php endforeach; ?>
				 <?php endif; ?> 
            </div>
            <div id="product-popup" class="product-popup mfp-hide">
			  	<div class="popup-wrapper product-box">
			  		
			  	</div>
			</div>
        </div>  
    </div>

<?php get_footer(); ?>
