<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">

    <title><?php wp_title(); ?></title>
    <?php wp_head(); ?>
  </head>

  <body <?php body_class( ); ?>>

    <!-- Static navbar -->
    
    <div class="container">
        <div class="container-wrapper">
            <header>

                <nav class="navbar navbar-default navbar-static-top">
                      <div class="navbar-header">
                            <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
                              <img src="<?php echo get_template_directory_uri(); ?>/assets/img/small-logo.png" alt="Logo">
                            </a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                  <span class="sr-only">Toggle navigation</span>
                                  <span class="icon-bar"></span>
                                  <span class="icon-bar"></span>
                                  <span class="icon-bar"></span>
                            </button>
                            <a href="<?php echo get_home_url(); ?>/cart/" class="cart" class="visible-xs"></a>
                      </div>
                        <?php 
                            wp_nav_menu( array(
                              'theme_location'    => 'primary',
                              'container'         => 'div',
                              'container_class'   => 'navbar-collapse collapse',
                              'container_id'      => 'navbar',
                              'menu_class'        => 'nav navbar-nav'
                            ) ); 
                        ?>
                </nav>
            </header>