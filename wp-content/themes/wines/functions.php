<?php

add_action( 'wp_enqueue_scripts', 'theme_scripts_styles' );
function theme_scripts_styles() { 

    $wp_styles = $GLOBALS['wp_styles'];

    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/vendor/bootstrap/css/bootstrap.min.css', array(), '' );
    wp_enqueue_style( 'slick-slider', get_template_directory_uri() . '/assets/vendor/slick/slick.css', array(), '' );
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/vendor/font-awesome/css/font-awesome.min.css', array(), '' );
    wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/assets/vendor/magnific-popup/magnific-popup.css', array(), '' );
    wp_enqueue_style( 'theme', get_stylesheet_uri(), array(), '' );
    
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/vendor/bootstrap/js/bootstrap.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'slick-slider', get_template_directory_uri() . '/assets/vendor/slick/slick.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/assets/vendor/magnific-popup/jquery.magnific-popup.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'maps', 'https://maps.googleapis.com/maps/api/js?sensor=true&v=3', array(), '', true );
    wp_enqueue_script( 'theme', get_template_directory_uri() . '/assets/js/theme.jquery.js', array('jquery'), '', true );
    
    $wp = array();
    
    //Add ajax url
    $wp['ajaxurl']  = admin_url('admin-ajax.php');
    $wp["themeurl"] = get_template_directory_uri();
    wp_localize_script( 'theme', 'wp', $wp );
}

add_action('wp_head','theme_head');
function theme_head(){ ?>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php }

add_action( 'after_setup_theme', 'theme_after_setup_theme' );
function theme_after_setup_theme() {
    register_nav_menu( 'primary', __( 'Main Navigation', 'wine' ) );

    add_image_size('slider-main', 1000, 440, true);
    add_image_size('product-loop', 100, 392, false);

    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-header', array(
        'width'         => 1200,
        'height'        => 252,
        'default-image' => get_template_directory_uri() . '/assets/content/featured-image.jpg',
    ));
}

function get_theme_categories() {
    $args = array(
        'hide_empty' => false,
        'order'      => 'ASC',
    );
    return get_terms( 'product_cat', $args );
}

function get_theme_products($servs) {
    $services = get_posts( array(
        'post_type' => 'product',
        'tax_query' => array(
            array(
                'taxonomy'  => 'product_cat',
                'field'     => 'term_id',
                'terms'     => $servs),
            )
        )
    );
    return $services;
}

function get_theme_product_price($prductid) {
    $product = new WC_Product(  $prductid );
    return $price = $prod->price . get_woocommerce_currency_symbol( $currency );;
}

function get_theme_stock_status($product_id) {
    $product_stock = wc_get_product($product_id); 
    return $product_stock->get_stock_quantity();
}

//add_theme_support( 'woocommerce' );

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}


//* Add stock status to archive pages
function envy_stock_catalog($product) {
    global $product;
    if ( $product->is_in_stock() ) {
        echo '<div class="stock" >' . $product->get_stock_quantity() . __( ' in stock', 'envy' ) . '</div>';
    } else {
        echo '<div class="out-of-stock" >' . __( 'out of stock', 'envy' ) . '</div>';
    }
}
add_action( 'woocommerce_after_shop_loop_item_title', 'envy_stock_catalog' );


// change add to cart text
add_filter( 'woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text' );  
function woo_archive_custom_cart_button_text() {
    return __( 'Add to basket', 'woocommerce' ); 
}


// Set a minimum number of products requirement before checking out
add_action( 'woocommerce_check_cart_items', 'spyr_set_min_num_products' );
function spyr_set_min_num_products() {
    // Only run in the Cart or Checkout pages
    if( is_cart() || is_checkout() ) {
        global $woocommerce;

        // Set the minimum number of products before checking out
        $minimum_num_products = 12;
        // Get the Cart's total number of products
        $cart_num_products = WC()->cart->cart_contents_count;

        // Compare values and add an error is Cart's total number of products
        // happens to be less than the minimum required before checking out.
        // Will display a message along the lines of
        // A Minimum of 20 products is required before checking out. (Cont. below)
        // Current number of items in the cart: 6   
        if( $cart_num_products %$minimum_num_products != 0 ) {
            // Display our error message
            wc_add_notice( sprintf( '<strong>A minimum of %s products, or a multiply of 12 products is required  for adding products to cart</strong>' 
                . '<br />Current number of items in the cart: %s.',
                $minimum_num_products,
                $cart_num_products ),
            'error' );
        }
    }
}


// Change View Cart Message

add_filter( 'wc_add_to_cart_params', 'jscarttorfq', 10, 1 );

function jscarttorfq( $localized ) {
    $localized['i18n_view_cart'] = __( 'MOQ  is  12 bottles <br /> 1 item added to cart', 'woocommerce' );
    return $localized;
}


// Get the excerpt

/**
 * Get the excerpt for a WP_Post by post ID.
 * 
 * @param int $post_id
 */

$wpse_excerpt = strip_tags($wpse_excerpt, wpse_allowedtags());
function wpse_allowedtags() {
// Add custom tags to this string
    return '<script>,<style>,<br>,<em>,<i>,<ul>,<ol>,<li>,<a>,<p>,<img>,<video>,<audio>'; 
}

// function get_excerpt_by_id( $post_id = 0 ) {
//     global $post;
//     $save_post = $post;
//     $post = get_post( $post_id );
//     setup_postdata( $post );
//     $excerpt = get_the_excerpt();
//     $post = $save_post;
//     wp_reset_postdata( $post );
//     return $excerpt;
// }

/*
* Gets the excerpt of a specific post ID or object
* @param - $post - object/int - the ID or object of the post to get the excerpt of
* @param - $length - int - the length of the excerpt in words
* @param - $tags - string - the allowed HTML tags. These will not be stripped out
* @param - $extra - string - text to append to the end of the excerpt
*/
function theme_get_excerpt_by_id($post, $length = 10, $tags = '<a><em><strong>', $extra = ' . . .') {
 
    if(is_int($post)) {
        // get the post object of the passed ID
        $post = get_post($post);
    } elseif(!is_object($post)) {
        return false;
    }
 
    if(has_excerpt($post->ID)) {
        $the_excerpt = $post->post_excerpt;
        return apply_filters('the_content', $the_excerpt);
    } else {
        $the_excerpt = $post->post_content;
    }
 
    $the_excerpt = strip_shortcodes(strip_tags($the_excerpt), $tags);
    $the_excerpt = preg_split('/\b/', $the_excerpt, $length * 2+1);
    $excerpt_waste = array_pop($the_excerpt);
    $the_excerpt = implode($the_excerpt);
    $the_excerpt .= $extra;
 
    return apply_filters('the_content', $the_excerpt);
}