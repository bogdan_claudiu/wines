(function($) {
	"use strict";
	
	var theme = {
		onReady : function(){

			$('.main-slider').slick({
				dots: false,
				arrows: false,
				infinite: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				draggable: false,
				fade: true,
			});

			$(".cta .btn-default, .cta .mask").on("click", function(e) {
				e.preventDefault();
				$(this).parent().parent().addClass("show-overlay");
			});

			$(".cta .close-overlay").on("click", function(e) {
				e.preventDefault();
				$(this).parent().parent().removeClass("show-overlay");
			});

			// back to top
			if ( ($(window).height() + 100) < $(document).height() ) {
			    $('.top-link-block').removeClass('hidden').affix({
			        // how far to scroll down before link "slides" into view
			        offset: {top:200}
			    });
			}

			$(".top-link-block").on('click', function(){
				$('html,body').animate({scrollTop:0},'slow');
				return false;
			})
			// end back to top

	       	// $('.buy').click(function(e) {
	        //   	e.preventDefault();
	        //  	addToCart($(this).data('prod-id'));
	        //   	alert($(this).data('prod-id'));
	        //   	return false;
	       	// });

	       	// open shop bottle in popup
	       	$('.open-big-image').magnificPopup({
	       		type:'image'
	       	});

	        $('.open-popup-link').magnificPopup({
			  	type:'inline',
			  	midClick: true,
			  	closeBtnInside:true,
			  	callbacks: {
				  	open: function() {
				  		var current_id = this.st.el.attr("data-id");
				      	var curent_product = this.st.el.parent().parent().parent().parent().clone();
				      	$(curent_product).find("h3").prepend('<img src="' + wp.themeurl + '/assets/img/cluster.jpg" alt="Cluster" />');
				      	$(curent_product).find(".open-big-image").replaceWith(function() { return this.innerHTML; });
				      	$(curent_product).find(".zoom-area").remove();
				      	$(curent_product).find("h3 a").replaceWith(function() { return this.innerHTML; });
				      	$(".product-popup .popup-wrapper").html("").append(curent_product);
				      	
				    },
				}
			});

			if ( $(".map-canvas").length > 0 ) {
				$(".map-canvas").each(function() {
					google.maps.event.addDomListener(window, 'load', theme.initMap( $(this).attr("id") ));
				});
			};

			$('.award a').magnificPopup({
				type: 'image',
				gallery: {
					enabled: true
				},

			});

			$('a[href*=#]:not([href=#])').not(".open-popup-link, a[role=tab]").click(function() {
				if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
					theme.scrollTo( $(this.hash) );
				}
			});

		},

		onResize : function() {

		},
		onLoad: function() {

			    if (window.location.hash) {
        
			        var target = $(location.hash);

			        $('html,body').animate({
			            scrollTop: target.offset().top - ( $('.navbar').outerHeight() + $('.blackbg').outerHeight() + 70)
			        }, 1000);
			        var current_tab =  location.hash.substr(1) ;
			        $('.nav-tabs a[href="#tab-' + current_tab + '"]').tab('show');
			        return false;
			        
			    }

		},

		scrollTo : function ( target ) {
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
					
				var $scrollTop = 0;

				if ( $(window).width() > 767 ) {
					$scrollTop = target.offset().top - $("#navbar").height() - $("#wpadminbar").height() + 1;
				} else {
					$scrollTop = target.offset().top - $(".navbar-header").height();
				}
				$('html,body').animate({
					scrollTop: $scrollTop
				}, 1000);
				return false;
			}
		},
		initMap : function( id ) {

			var is_draggable = true;

			if ( $(window).width() < 767 ) {
				is_draggable = false;
			}

			var lat = $("#" + id).attr("data-lat");
			var lng = $("#" + id).attr("data-lng");
			var zoom = parseInt($("#" + id).attr("data-zoom"));
			var mapCanvas 	= document.getElementById( id );
			var mapOptions 	= {
				center: new google.maps.LatLng(lat, lng),
				disableDefaultUI: false,
				zoomControl: true,
	     		mapTypeControl: false,
				scaleControl: false,
				streetViewControl: false,
				rotateControl: false,
				draggable: is_draggable,
				zoom: zoom,
			}

			var map = new google.maps.Map( mapCanvas, mapOptions);

			var marker 	= null;

			marker = new google.maps.Marker({
				animation: google.maps.Animation.DROP,
				position: new google.maps.LatLng( $("#" + id).attr("data-lat"), $("#" + id).attr("data-lng") ),
				map: map,
			});

			marker.setMap(map);

			
		},	
	};

	$(document).ready( theme.onReady );
	$(window).load( theme.onLoad );
	$(window).resize( theme.onResize );

})(jQuery);