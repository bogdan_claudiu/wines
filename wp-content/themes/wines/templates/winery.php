<?php 
/**
 * Template Name: Winery Template
 */
get_header(); ?>
        </div>
        <div class="main-content">
            <div class="main-content-wrapper">
                <div class="row">
                    <div class="col-sm-6 visible-xs">

                        <img class="img-responsive center-block logo-vinum" src="<?php echo get_template_directory_uri()?>/assets/img/winum-logo.png" alt="Logo Vinum">
                        <img class="img-responsive center-block" src="<?php echo get_template_directory_uri()?>/assets/img/cluster.png" alt="Cluster">
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <a href="#" class="wine-picture left">
                            <span class="image"></span>
                            <span class="mask"></span>
                        </a>
                        <h3 class="picture-text">TERRA ZORM
                            <span>Winery</span>
                        </h3>
                    </div>
                    <div class="col-sm-6 hidden-xs">

                        <img class="img-responsive center-block logo-vinum" src="<?php echo get_template_directory_uri()?>/assets/img/winum-logo.png" alt="Logo Vinum">
                        <img class="img-responsive center-block" src="<?php echo get_template_directory_uri()?>/assets/img/cluster.png" alt="Cluster">
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <a href="#" class="wine-picture right">
                            <span class="image"></span>
                            <span class="mask"></span>
                        </a>
                        <h3 class="picture-text right">SNDBBS DKJRFNE
                        <span>SNDBBS DKJRFNE</span>
                        </h3>
                    </div>
                </div>
                <div class="main-intro">
                    <h3><span> A tradition that goes back to centuries and continues to win the taste of many throughout the years. Taste and Enjoy a terroir discovered by the Romans and transformed into 21st Century wines.</span></h3>
                </div>
                <div class="intro-content">
                    <h3>The Estate</h3>
                    <p>Cultivating vineyards in the traditional way, for several centuries in the Eastern part of Austria, has and continues to be the Winemaker's recipe that made him win several golden prizes over the years. Located in Honstein and Grosshoflein, the estates enjoy optimal benefits from the sunny Burgenland and the fresh air breeze that blows over the vineyards and gives the wine produced its full potential. Karl and Hermi Gerdenitsch have continued what their ancestors have started since the 1600s. While utilizing the long traditional way of wine production, they expanded the potential and modernized it, in such a way that the wine has maintained both its excellent quality and taste. Each year in May and until the wine harvest in autumn, the wines are nurtured by hand so that the grapes enjoy maximum benefits and the full potential of the wine is unleashed. </p>
                    <h3>The Vineyards</h3>
                    <p>The vineyards of the Gardenitsch family lie between Grosshoflein and Hornstein, along the southwest slopes of the Leith-mountain range.</p>
                    <h3>Grosslage Grosshoflein</h3>
                    <p>The wine region of Grosslage Grosshoeflein boasts a terroir of sandy clay and rich black humus. Gentle breezes combined with sun-drenched days and warm nights create a climate that builds the typical character of flavorful white wines such as Muskateller, Osterreich-Weiss, Welschriesling as well as those of the full-body reds.
                    (Blaudrankisch, Merlot, Zweigelt) This unique micro-climate especially favors noble sweet wines.</p>
                    <h3>Hornstein</h3>
                    <p>Sauvignon Blan and Welschriesling flourish on this sandy and pebbly clay terroir.</p>
                    <h3 class="copyright">TERRAZORM</h3>
                </div>
            </div>
        </div>  
    </div>
<?php get_footer(); ?>    