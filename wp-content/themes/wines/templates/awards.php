<?php 
/**
 * Template Name: Awards Template
 */
get_header(); ?>

	<div class="main-slider">
        <div>
            <img class="img-responsive" src="<?php echo get_template_directory_uri()?>/assets/content/slide-1.jpg" alt="Slide 1">
        </div>
    </div>

    <div class="main-content" id="awards-content">
        <div class="main-content-wrapper">

        	<h3>Awards</h3>

			<div class="awards">
	        	<div class="row">
	        		<div class="col-sm-3">
	        			<div class="award">
	        				<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/content/newcomer 2014.jpg" alt="Award">
	        				<a class="mask" href="<?php echo get_template_directory_uri(); ?>/assets/content/newcomer 2014.jpg"><i class="fa fa-search"></i></a>
	        			</div>
	        		</div>
	        		<div class="col-sm-3">
	        			<div class="award">
	        				<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/content/cert_newcomer 2015.jpg" alt="Award">
	        				<a class="mask" href="<?php echo get_template_directory_uri(); ?>/assets/content/cert_newcomer 2015.jpg"><i class="fa fa-search"></i></a>
	        			</div>
	        		</div>
	        		<div class="col-sm-3">
	        			<div class="award">
	        				<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/content/cert_salon.jpg" alt="Award">
	        				<a class="mask" href="<?php echo get_template_directory_uri(); ?>/assets/content/cert_salon.jpg"><i class="fa fa-search"></i></a>
	        			</div>
	        		</div>
	        		<div class="col-sm-3">
	        			<div class="award">
	        				<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/content/cert_wine_champ.jpg" alt="Award">
	        				<a class="mask" href="<?php echo get_template_directory_uri(); ?>/assets/content/cert_wine_champ.jpg"><i class="fa fa-search"></i></a>
	        			</div>
	        		</div>
	        	</div>
	        </div>

        </div>
    </div>



<?php get_footer(); ?> 