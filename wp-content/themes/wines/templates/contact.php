<?php 
/**
 * Template Name: Contact Template
 */
get_header(); ?>

<main>
	
	<?php if ( $google_map = get_field('google_map') ) :  ?>
		<div id="map_canvas_1" class="map-canvas single-map-canvas" data-zoom="<?php echo get_field('zoom'); ?>" data-lat="<?php echo $google_map['lat']; ?>" data-lng="<?php echo $google_map['lng']; ?>" data-content="<?php echo $address; ?>"></div>
	<?php endif; ?>

	<h3><?php the_title(); ?></h3>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; endif; ?>

</main>

<?php get_footer(); ?>