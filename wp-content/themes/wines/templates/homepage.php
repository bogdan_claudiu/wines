<?php 
/**
 * Template Name: Home Template
 */
get_header(); ?>
    
    <main>
        <div class="main-slider">
            <div>
                <img class="img-responsive" src="<?php echo get_template_directory_uri()?>/assets/content/slide-1.jpg" alt="Slide 1">
            </div>
        </div>
    </main>
    
<?php get_footer(); ?>    