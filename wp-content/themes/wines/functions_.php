<?php

add_action( 'wp_enqueue_scripts', 'theme_scripts_styles' );
function theme_scripts_styles() { 

    $wp_styles = $GLOBALS['wp_styles'];

    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/vendor/bootstrap/css/bootstrap.min.css', array(), '' );
    wp_enqueue_style( 'slick-slider', get_template_directory_uri() . '/assets/vendor/slick/slick.css', array(), '' );
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/vendor/font-awesome/css/font-awesome.min.css', array(), '' );
    wp_enqueue_style( 'mrb', get_template_directory_uri() . '/assets/vendor/magnific-popup/magnific-popup.css', array(), '' );
    wp_enqueue_style( 'theme', get_stylesheet_uri(), array(), '' );
    
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/vendor/bootstrap/js/bootstrap.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'slick-slider', get_template_directory_uri() . '/assets/vendor/slick/slick.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/assets/vendor/magnific-popup/jquery.magnific-popup.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'theme', get_template_directory_uri() . '/assets/js/theme.jquery.js', array('jquery'), '', true );
    
    $wp = array();
    
    //Add ajax url
    $wp['ajaxurl']  = admin_url('admin-ajax.php');
    $wp["themeurl"] = get_template_directory_uri();
    wp_localize_script( 'theme', 'wp', $wp );
}

add_action('wp_head','theme_head');
function theme_head(){ ?>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php }

add_action( 'after_setup_theme', 'theme_after_setup_theme' );
function theme_after_setup_theme() {
    register_nav_menu( 'primary', __( 'Main Navigation', 'wine' ) );

    add_image_size('slider-main',1000, 440, true);
    add_image_size('product-loop',77, 302, true);

    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-header', array(
        'width'         => 1200,
        'height'        => 252,
        'default-image' => get_template_directory_uri() . '/assets/content/featured-image.jpg',
    ));
}

function get_theme_categories() {
    $args = array(
        'hide_empty' => false,
        'order'      => 'ASC',
    );
    return get_terms( 'product_cat', $args );
}

function get_theme_products($servs) {
    $services = get_posts( array(
        'post_type' => 'product',
        'tax_query' => array(
            array(
                'taxonomy'  => 'product_cat',
                'field'     => 'term_id',
                'terms'     => $servs),
            )
        )
    );
    return $services;
}

function get_theme_product_price($prductid) {
    $product = new WC_Product(  $prductid );
    return $price = $prod->price . get_woocommerce_currency_symbol( $currency );;
}

function get_theme_stock_status($product_id) {
    $product_stock = wc_get_product($product_id); 
    return $product_stock->get_stock_quantity();
}

//add_theme_support( 'woocommerce' );

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}


// //* Add stock status to archive pages
// function envy_stock_catalog($product) {
//     global $product;
//     if ( $product->is_in_stock() ) {
//         echo '<div class="stock" >' . $product->get_stock_quantity() . __( ' in stock', 'envy' ) . '</div>';
//     } else {
//         echo '<div class="out-of-stock" >' . __( 'out of stock', 'envy' ) . '</div>';
//     }
// }
// add_action( 'woocommerce_after_shop_loop_item_title', 'envy_stock_catalog' );


/**
 * Redirect the Continue Shopping URL from the default (most recent product) to
 * a custom URL.
 * Place this code snippet in your theme's functions.php file.
 */
function custom_continue_shopping_redirect_url ( $url ) {
    $url = "http://www.woothemes.com"; // Add your link here
    return $url;
}
add_filter('woocommerce_continue_shopping_redirect', 'custom_continue_shopping_redirect_url');



wp_dequeue_script( 'wc-add-to-cart-variation' );
wp_dequeue_script( 'wc-cart' );
