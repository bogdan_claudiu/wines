<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wines');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-E3GR{j3}d-mvVT8 cDlxK:8rd5_xp#:>OmnyiAvP%^Oe`0HK/}OULq@[pXJR$|>');
define('SECURE_AUTH_KEY',  'r3p%19>v,2lziM}Mv2+-v~*|Fym]L<HhZR*D-tG`HfBr}|Ih8sOhbBf=_PCbs:zp');
define('LOGGED_IN_KEY',    'Sfoj2U8B~=<v%}`eR:I/&e9yD8g;MeXj-E0VTHP$f_!7a5+&I!EG)<$ znIZu~e:');
define('NONCE_KEY',        '54a[nQb_bL=z|lq?da*<^--gkR}tC3vZ6B([}]OXcbW3i>>oB%b_Xte/D[6C*yCd');
define('AUTH_SALT',        'eYqp,[l#Rh5B.dw1VB]a</@(!b?6]I)%[p=w-=+)eGbz5FokzdA<<kX]Z&qC30yE');
define('SECURE_AUTH_SALT', 'n(mbt@W5D~RwJhWRPVYzze^`3H.sFO{..{T5|DB _fsDAG9GKXzWWn1s8~<d[R~m');
define('LOGGED_IN_SALT',   '%c>GoctX!rHOaaeuueBPhcdCpBxP>_z1QXnN|CM R|C=cb:r.0(nX*t6[/)EqcK$');
define('NONCE_SALT',       'u {7Gb|*;bA0]=>!_@`&{O?B|YhOn}G@}Dq2`#d{v^@O+|*FW1-^4Del,SZ9-W5?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
